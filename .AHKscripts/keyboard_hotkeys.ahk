;MIT License

;Copyright (c) 2020 Seve Tessarin

;Permission is hereby granted, free of charge, to any person obtaining a copy
;of this software and associated documentation files (the "Software"), to deal
;in the Software without restriction, including without limitation the rights
;to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;copies of the Software, and to permit persons to whom the Software is
;furnished to do so, subject to the following conditions:

;The above copyright notice and this permission notice shall be included in all
;copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;SOFTWARE.

#Persistent
#SingleInstance, Force
; Force or Ignore
; partial match of window title
SetTitleMatchMode, 2

; Clean clipboard
;+#v::
;Run, cmd /c "echo off | clip"
;return


; pin to the top
^+SPACE:: 
gosub, PintoTop
return

PintoTop:
Winset, Alwaysontop, , A
return 

;text to voice with win-s for speak

^#v:: ;ctrl+win+s
clp=%clipboard%
ComObjCreate("SAPI.SpVoice").Speak(clp)
return 

;; mintty hotkeys
#If WinActive("ahk_exe mintty.exe")

;; ctrl v
;;^v::
;;needs option keys copy/paste shortcut from mintty paste
;SendInput +{Insert} 
;return
;; ctrl c
;^c::
;;needs option keys copy/paste shortcut from mintty copy
;SendInput ^{Insert}
;return


Capslock::
SendInput {Esc}{Esc}
return
Esc::
SendInput {Esc}{Esc}
return
;alt q
!q::
Send {:}{q}{!}
return



; use numpad to move in tmux, dont know if combination of 3 keystrokes works
Numpad0::^q
NumpadDiv::
Send ^{q}{''}
return
; not working % special meaning
NumpadMult::
Send ^{q}{"}
return
NumpadSub::
Send ^{q}{&}
return
NumpadAdd::
Send ^{q}{c}
return
Numpad1::
Send ^{q}{1}
return
Numpad2::
Send ^{q}{2}
return
Numpad3::
Send ^{q}{3}
return
Numpad4::
Send ^{q}{4}
return
Numpad5::
Send ^{q}{5}
return
Numpad6::
Send ^{q}{6}
return
Numpad7::
Send ^{q}{7}
return
Numpad8::
Send ^{q}{8}
return
Numpad9::
Send ^{q}{9}
return

;;; copy to tmux buffer
;^+y::
;Send !w  ; copy selection
;return
;;; paste from tmux buffer
;^+p::
;Send  ^q] ; copy selection
;return


;; awesome!!!!!!
;; check selection in the options 
;; tmux you can not c/p like in linux with a mouse selection hence
+~LButton:: ; Make the left mouse button a hotkey, but allow it to function as normal.
Coordmode, Mouse, Screen
MouseGetPos, xA, yA
Keywait, LButton, L
MouseGetPos, xB, yB
SendInput ^{Insert} ; Copy here is ctrl-insert
If (Abs(xA-xB) + Abs(yA-yB) > 3)
{
   
   Clip0 = %ClipBoardAll%
   clipboard = %clipboard%   ; Convert the cliboard contents to plain text.
 ; MsgBox,%clipboard%
}
else{
	if (A_TimeSincePriorHotKey < DllCall("GetDoubleClickTime") && A_PriorHotkey = A_ThisHotkey){		; Double click
    clipboard = %clipboard%   ; Convert the cliboard contents to plain text.
	}}  
return

#If

    



