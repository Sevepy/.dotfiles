# @seve_py bashrc file
# Handles mingw64/msys2 subsystems with Python installed from windows msi 64-bit
# Prompt shows if shell started with elevated privileges

########################### 1 Functions ###########################################
# fix win32 path
showtextfile_inpath()
{
    whereis $1 | sed -e 's/\\/\\\\/g' | xargs.exe cat {}s/\\/\\\\/g

}


pyclean () {
    find . -regex '^.*\(__pycache__\|\.py[co]\)$' -delete
}

# check if the shell has been started with run as Administrator
_amiadministrator() {
ID=$(id);
if [[ $ID =~ "Administrator" ]]
then
    printf "Administrator"
else
    printf  $(whoami)
fi
}

# Determine git branch.
_parse_git_branch() {
   git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'    
}
_pwd() {
    pwd
}


_new_line() {
    printf "\n "
}

# see .inputrc for keybinding
function fgbg() {
    fg
}

source ~/.git-prompt.sh
####################################################################################

[ -z ${PLATFORM+x} ] && export PLATFORM=$(uname -s)

export EDITOR='vim'

# bash color schema
LS_COLORS='di=94:fi=0:ln=01;31;46:pi=5:so=5:bd=5:cd=5:or=58;101:mi=17;103:ex=35:*.py=90'

export LS_COLORS
umask 0002

#######################################User's binaries###############################
export PATH="~/bin:${PATH}"
#####################################################################################

########################################COLORS########################################
RED="\[\e[31m\]"
RESET="\[\e[0m\]"
YELLOW="\[\e[33m\]"
GREEN="\[\e[32m\]"
BLU="\[\e[34m\]"
WHITE="\[\e[37m\]"
export PYTHONIOENCODING=utf8
#######################################graphviz#######################################
export PATH="/c/graphviz-2.38/release/bin:${PATH}"
export LD_LIBRARY_PATH="/c/graphviz-2.38/release/lib::${LD_LIBRARY_PATH}"
######################################################################################

#######################################node###########################################
export PATH="${HOME}/workspace/toolbox/apps/node:${PATH}"
######################################################################################

#####################################LO########################
alias soffice="/c/Program\ Files/LibreOffice/program/soffice.exe" 

alias svgbob='$HOME/AppData/Local/Programs/Python/Python311/Lib/site-packages/markdown_svgbob/bin/svgbob_0.5.5_x86_64-Windows.exe'
###############################################################


###################################################################
######################
## Managing dotfiles without symlinks. Windows does not allow symlinks unless you are Administrator.
## https://github.com/anandpiyer/.dotfiles/blob/master/.dotfiles/README.md

alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles config --local status.showUntrackedFiles no

## gnu diction
alias diction='$HOME/AppData/Local/Programs/WinDiction/diction.exe -s -f $HOME/AppData/Local/Programs/WinDiction/en_GB '

## dictionary TODO change to a function
alias dict='curl dict://dict.org/d:'


alias winpython='winpty python'
#------------------------------------------------------
alias ls='ls --color=auto'

alias grep='grep --color=auto' 

alias lsl='ls -apl --color=never |less'

# gnuplot included in octave for Windows

alias gnuplot='winpty ~/workplace/octave/octave-8.2.0-w64/mingw64/bin/gnuplot.exe'
alias octave='winpty ~/workplace/octave/octave-8.2.0-w64/mingw64/bin/octave-cli.exe'

## fix the stdout not a tty error
alias wpty='winpty -Xallow-non-tty'

# node repl
alias node='node.exe'

# alias for cheat.sh client https://github.com/chubin/cheat.sh
alias cspy='cht.sh python'
alias csjs='cht.sh javascript'
alias cs='cht.sh'
alias e9='winpty ~/Vim/vim90/vim.exe'
alias fzf='winpty ~/bin/fzf.exe'
case  $(uname | tr '[:upper:]' '[:lower:]') in
    mingw*)
        echo "MINGW@"${USER} - $(_amiadministrator)
        # python console
        export PATH="/c/users/${USER}/appdata/local/programs/python/python311:${PATH}"
        # ipython console
        export PATH="/c/users/${USER}/appdata/local/programs/python/python311/Scripts:${PATH}"
        # pandoc
        export PATH="/c/users/${USER}/appdata/local/programs/windiction:${PATH}"
        ## PATH constructed in reverse order, last in first out
        
        export PATH="/c/users/${USER}/workspace/toolbox/bin:${PATH}"
        export PATH="/c/msys64/clang64/bin:${PATH}"
        export PATH="/c/users/${USER}/workplace/v:${PATH}"
        ## jq binary
        export PS1="$YELLOW"'$(_amiadministrator)$(__git_ps1 " (%s)")$(_new_line)'"$BLU\w>$RESET"
        export PATH="/c/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/Llvm/x64/bin:${PATH}" 
        #"no rlwrap"
        alias pandoc='winpty pandoc.exe'
        ## dot 
        alias dot='wpty dot'

        ## binary from https://github.com/BurntSushi/ripgrep/releases ripgrep-12.1.1-x86_64-pc-windows-gnu.zip

        alias rg='rg.exe'

        #alias sqlplus="rlwrap -i -f ~/.sqlplus_history -H ~/.sqlplus_history -s 30000 sqlplus --"
        #touch ~/.sqlplus_history

        ### alias for taskwarrior
        alias task='winpty task'

        ## On windows remember to lispselector.bat and select SBCL
        ## it creates the username/maxima directory as well
        
        #alias maxima='winpty rlwrap --file=${HOME}/.vim/plugged/vim-wcalc/syntax/acmaxima.txt  /c/maxima-5.42.2/bin/maxima.bat'

        alias ipython='winpty ipython'
        ### imagemag from Win64 dynamic at 16 bits-per-pixel component has imdiplay.exe
        alias imdisplay='wpty imdisplay.exe'
        alias cmd='alacritty.exe --command cmd.exe'
        ##### Python interactive Windows path ############
        export PYTHONSTARTUP="C:\\Users\\${USER}\\.winpythonstartup.py"
        export LD_LIBRARY_PATH="~/workplace/msys2/mingw64/include:~/workplace/msys2/mingw64/lib:${LD_LIBRARY_PATH}"
    ;;
    msys*)
   
        echo "MSYS2@"${USER} - $(_amiadministrator) 
        export PS1="$GREEN"'$(_amiadministrator)$(__git_ps1 " (%s)")$(_new_line)'"$BLU\w>$RESET"
        ## gsl installed from compiled binary

        export LD_LIBRARY_PATH="~/workplace/msys2/mingw64/gsl/include/gsl:~/workplace/msys2/mingw64/lib:${LD_LIBRARY_PATH}"
        ##### Python interactive ############
        export PYTHONSTARTUP="${HOME}/.winpythonstartup.py"

        ### use mingw64 binary
        export PATH="/c/msys64/mingw64/bin:${PATH}"


        ;;
esac

alias posh='winpty -Xallow-non-tty powershell'

alias recover_unstaged_del='git ls-files -d | sed -e "s/\(.*\)/'\1'/" | xargs git checkout --'

alias recover_staged_del='git status | grep "deleted:" | awk "{print $2}" | xargs git checkout --'

alias bind='bind -P |less'

alias tmux='tmux.exe -2'
## source .bashrc each time a change dir to check in ./bin exists
alias calc='wcalc.exe -P10'

alias idle='python -m idlelib'
## install idle extension with pip install idlexlib
alias idlex='idlex3'

alias vi='vim'
alias vitest='vim -V9vim.log'

##rg for v
alias rgv="rg --type-add 'v:*.{vsh,v}' -tv "
# same as ctrl-D
alias E='exit'

alias pubsnote='vim note:'


