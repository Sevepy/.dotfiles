alert("custom.js")

$.getScript( "https://tessarinseve.pythonanywhere.com/js/sectionnumbering.js" );

$.getScript("https://tessarinseve.pythonanywhere.com/staticweb/nb_caption.js")

Jupyter.keyboard_manager.command_shortcuts.add_shortcut('t,c', {
help: 'Create Table of contents',
help_index: 't,c',
handler: function (event) {
    $('div#toc').html("")
    $("h1, h2").each(function(i) {
                    var current = $(this);
                    current.attr("id", "title" + i);
                    /* Create a empty markdown cell (press m) and type:
                    <div id="toc">
                    </div>
                    */
                    $("#toc").append("<a id='link" + i + "' href='#title" + i + "' title='" + current.attr("tagName") + "'>" + current.html() + "</a></br>");
                    
    });

    // scrolling to toc 
    var pos = $("#toc").offset().top;
    $("div.toc").scrollTop(pos);
    return false;
}
});

IPython.keyboard_manager.command_shortcuts.add_shortcut('Ctrl-k', {
    help : 'move up selected cells',
    help_index : 'jupyter-notebook:move-selection-up',
    handler : function (event) {
        IPython.notebook.move_selection_up();
        return false;
    }}
);

IPython.keyboard_manager.command_shortcuts.add_shortcut('Ctrl-j', {
    help : 'move down selected cells',
    help_index : 'jupyter-notebook:move-selection-down',
    handler :  function (event) {
        IPython.notebook.move_selection_down();
        return false;
    }}
);
