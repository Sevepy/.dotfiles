" Vim compiler file
" Compiler:             Python Compiler embedded Ipython console
" Maintainer:           @seve_py
" Latest Revision:      2024-05-06 

if exists('current_compiler')
  finish
endif
let current_compiler = 'python'

if exists(':CompilerSet') != 2              " older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet makeprg=ruff.exe\ check\ %
CompilerSet errorformat=%f:%l:%c:%m

abb pybp embed()
abb Ipi from IPython import embed
