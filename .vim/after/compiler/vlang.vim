" Vim compiler file
" Compiler:             V Compiler
" Maintainer:           @seve_py
" Latest Revision:      2023-03-16
if exists('current_compiler')
  finish
endif
let current_compiler = 'vlang'

if exists(':CompilerSet') != 2              " older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

"                    path to vlang compiler       
CompilerSet makeprg=C:\\Users\\Seve\\workspace\\toolbox\\apps\\v\\v.exe\ %

CompilerSet errorformat=%f:%l:%c:%m
