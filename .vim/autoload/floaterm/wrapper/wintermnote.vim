
function! floaterm#wrapper#wintermnote#(cmd, jobopts, config) abort

  let g:floaterm_wintype='float'
  let g:floaterm_position='right'
  let a:jobopts.on_exit = funcref('s:wintermnote_callback')
  return [v:false, 'C:\\Users\\Seve\\AppData\\Local\\Programs\\WinTermNote\\wintermnote_terminal.exe --external']
endfunction

function! s:wintermnote_callback(job, data, event, opener) abort
  sleep 100m
  let newbuf = system("more ".fnamemodify('~',':p')."\\.wintermnote_mr")
  if len(newbuf)>1
    exec(":e ".newbuf)
  endif
endfunction

