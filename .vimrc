"" **************************************************************************
if has('win32') || has('win64')
    set runtimepath-=~\vimfiles
    set runtimepath-=~\vimfiles\after
    set runtimepath-=~\Vim\vim90\compiler
    set runtimepath^=~\.vim
    set runtimepath-=~\vimfiles\after
    set runtimepath+=~\.vim\after
    set runtimepath^=~\.vim\pack\dist
    set termguicolors
    set shellslash
    let g:airline_powerline_fonts = 0
else
    if exists('$SHELL')
        set shell=$SHELL
    else
        set shell=/bin/sh
    endif
endif


"" --------------------------------------------------------------------------

"*****************************************************************************
"" 0 Vim-Plug core
"*****************************************************************************


if has('win32') || has('win64')
        let vimplug_exists=expand('~/vimfiles/autoload/plug.vim')
    else
        let vimplug_exists=expand('~/.vim/autoload/plug.vim')
endif

if !filereadable(vimplug_exists)
  if !executable("curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent exec "!\curl -fLo " . vimplug_exists . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

" Required:
silent call plug#begin(expand('~/.vim/plugged'))
""---------------------------------------------------------------------------

"*****************************************************************************
"" 1 Plugins
"*****************************************************************************
Plug 'sevehub/recentdirs.vim'



" scratch buffer
Plug 'mtth/scratch.vim'

Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-obsession'
" git status integrated in the statusline
Plug 'tpope/vim-fugitive'

Plug 'tpope/vim-surround'

" required by fugitive (:Gbrowse)
" Plug 'tpope/vim-rhubarb'

" Git diff in the side column
Plug 'airblade/vim-gitgutter'

" Dark color schemes
Plug 'NLKNguyen/papercolor-theme'

" easy comment/uncomment line
Plug 'scrooloose/nerdcommenter'

"math unicode abbreviation"
Plug 'joom/latex-unicoder.vim'



" airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" automatic closing of quotes, parenthesis, brackets, etc.,
Plug 'Raimondi/delimitMate'

" browse tags on a sidebar
Plug 'majutsushi/tagbar'

" Range, pattern and substitute preview for Vim
Plug 'markonm/traces.vim'

" minifuzzy.vim vim9 only (support for rust fd)
" not working with shell sets to powershell
Plug 'sevehub/fdminifuzzy.vim'

" rest console
Plug 'diepm/vim-rest-console'

" Align text
Plug 'godlygeek/tabular'

"" Vim-Session
Plug 'xolox/vim-misc'
"" Plug 'xolox/vim-session'
Plug 'sevehub/vim-notes'

""" Resize window with CTRL-E
Plug 'vim-scripts/winresizer.vim'

"" words and line Highlighter
Plug 'azabiong/vim-highlighter'

""  https://tessarinseve.pythonanywhere.com/nws/2021-10-11.wiki.html
Plug 'https://gitlab.com/Sevepy/vim-w3m'


"" wiki.vim website editing
"" Plug 'https://gitlab.com/sevepy/wiki.vim'
Plug 'sevehub/wiki.vim'
"" Calendar vim
Plug 'mattn/calendar-vim'

"" 
Plug 'voldikss/vim-floaterm'

"" dev vim-tig
"" Plug '~/.vim/plugged/vim-tig'
"" Plug 'https://gitlab.com/Sevepy/vim-tig'

"" dev vim-wterminals
Plug 'https://sevepy@bitbucket.org/sevepy/vim-mterminals.git'


"" vlang
Plug 'ollykel/v-vim'

"" typist
"" Plug 'sevehub/typst.vim'
Plug 'sevehub/typstpowershell'
Plug 'sevehub/vim9psgrep'

"" Python Ruff-Lsp
Plug 'yegappan/lsp' 
Plug 'tandrewnichols/vim-determined'

"" --------------------------------------------------
"" Python Ipython
"" --------------------------------------------------

"" Syntax support for Vim.
Plug 'raimon49/requirements.txt.vim', {'for': 'requirements'}

" notebook<---->md replaces Plug 'goerz/ipynb_notedown.vim', requires pip install jupytext
Plug 'goerz/jupytext.vim'

"" Syntax plugin for .m octave files
Plug 'https://gitlab.com/Sevepy/octave.vim'


"" Project Management Taskfalcon yaml
Plug 'https://sevepy@bitbucket.org/sevepy/vim-taskfalcon.git'

""""""""""""""""""""""""coc plugin""""""""""""""""""""""""""""""""""""""""""""
" Use release branch
" followed by :
" CocInstall coc-python3
" CocInstall coc-html
" CocConfig add {} json empty dictionary
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"*****************************************************************************

"" Include user's current directory plugins list
if filereadable(expand("./.vimrc.local.bundles"))
  source ./.vimrc.local.bundles"
  let g:local_plugins= 1
else
  let g:local_plugins= 0
endif

call plug#end()


"*****************************************************************************

"" remind me if plugins are present in the working dir
" if g:local_plugins
    "echo "Project has some local plugins "
" endif

"" Include user's working config
if filereadable(expand("./vimrc.local"))
  source ./vimrc.local
endif

" Required:
filetype plugin indent on

" Gvim not happy with these
"autocmd BufWinLeave *.* mkview
"autocmd BufWinEnter *.* silent loadview


"*****************************************************************************
"" 2 Mappings
"*****************************************************************************

" source .vimrc
nnoremap <leader>rc <esc>:so $MYVIMRC<CR>


"" clear search highlight
nnoremap <silent><leader>nh <esc>:noh<cr><esc>
nnoremap <silent> <C-l> :nohl<CR><C-l>
"" remap bufxplorer
nnoremap <leader>bb <esc>:BufExplorer<cr>

"" remap file explorer
nnoremap <leader>e <esc>:Lexplore<cr>
"" vim dispatch for mingw64 and gvim
nnoremap <leader>ee <esc>:Start start .<cr>

execute "set <A-j>=\033j"
execute "set <A-k>=\033k"

nnoremap <A-k> <C-u><C-u>zzzv
nnoremap <A-j> <C-d><C-d>zzzv

" Search mappings: going to the next item will center on the line it has found.
nnoremap n nzzzv
nnoremap N Nzzzv

" X - backspace
nnoremap <BS> X

" spacebar insert space
nnoremap <space> i <esc>

" Add empty line above and below
nnoremap + i<cr><esc>
nnoremap - ddkk<cr><esc>

" insert diagram see :digraphs for full list
" http://www.alecjacobson.com/weblog/?p=443
nnoremap <C-k> i<C-k>

" terminal easy log out
tnoremap <Esc> <C-\><C-n> :q!

"mimic ctr-c in visual mode for copy
vnoremap <C-c> "+y

"  moving a split into a tab
nnoremap <silent> <leader>t <esc><c-w>T<cr>

"" Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

"" buffers
nnoremap <leader>bn :bnext<CR>:redraw<CR>:ls<CR>
nnoremap <leader>bp :bprevious<CR>:redraw<CR>:ls<CR>
nnoremap <leader>b :buffers<CR>

"" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

"" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" open link in the browser
" adapted from https://thevaluable.dev/vim-advanced/
nnoremap gW :silent :execute
            \ "!start" expand("<cfile>") " &"<cr>

"" Split horiz and vertical
nnoremap <Leader>h :<C-u>split<CR>
nnoremap <Leader>v :<C-u>vsplit<CR>

" terminal emulation
nnoremap <silent> <leader>sh :terminal<CR>

"" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>

noremap <Leader>ed :e <C-R>=expand("%:p:h") . "/" <CR>

noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>
" Redo with U instead of Ctrl+R
noremap U <C-R>
"*****************************************************************************
"" 3 Basic Setup
"*****************************************************************************"

" Map leader to \
let mapleader="\\"

" let g:homedir=shellescape(fnamemodify('~', ':p'))
let g:homedirw32 = fnamemodify('~',':p')
set background=dark

set nofoldenable
"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set ttyfast

""set notimeout nottimeout
set timeout timeoutlen=1000 ttimeoutlen=200

"" Fix backspace indent
set backspace=indent,eol,start "same backspace in insert mode

filetype plugin indent on
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

set ai " set auto-indenting
set autoread

""  no Hidden buffers
set nohidden

"" Searching
set hlsearch   " highlight searches
set incsearch  " incremental search
set ignorecase " ignore case
set smartcase
set showmatch  " jump to matches
set fileformats=unix,dos,mac

""" spellcheck
set spelllang=en
set spell

" share vim and win clipboard
set clipboard+=unnamed

"" Copy/Paste/Cut
" set paste
set pastetoggle=<Insert>
set nopaste
"set go+=a ?
set showcmd " display incomplete commands


set exrc " source folder specific .vimrc

"*****************************************************************************
"" 4 Visual Settings
"*****************************************************************************
syntax on
set ruler "show the current row and col
set rnu
set nu "show line number
let no_buffers_menu=1
silent! colorscheme PaperColor
set mouse=r

set noswapfile
set mousemodel=popup
set t_Co=256
set guioptions=egmrtiT
set gfn=Monospace\ 10

set viminfofile=C:\\Users\\Seve\\Vim\\viminfo\\_viminfo 
"set viminfo+=nC:\\Users\\Seve\\Vim\\viminfo\\.viminfo
"let &viminfofile=findfile('.viminfo','.;')

"This command makes Vim look for a .viminfo file in the current directory or any parent directory. If it finds one, it uses that file; otherwise, it defaults to the standard .viminfo file
" Turn off spell checking in the quickfix window
autocmd FileType qf setlocal nospell

" Turn off spell checking in the location list window
autocmd FileType location setlocal nospell


"" remove splash screen
set shm+=I
if has("gui_running")
  if has("gui_mac") || has("gui_macvim")
    set guifont=Menlo:h12
    set transparency=7
  endif
else

  if $COLORTERM == 'gnome-terminal'
    set term=gnome-256color
  else
    if $TERM == 'xterm'
      set term=xterm-256color
    endif
  endif

endif


if &term =~ '256color'
  set t_ut=
endif

"" Disable the blinking cursor.
set gcr=a:blinkon0
set scrolloff=3 "keep 3 lines when scrolling
" set cursorline
" set terminal type

set thesaurus+=~/.vim/thesaurus/mthesaur.txt

"" Status bar
set laststatus=2

set modeline
set modelines=10

set title
set titleold="Terminal"
set titlestring=%F

set statusline=%{v:register}%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\
if exists("*fugitive#statusline")
  set statusline+=%{fugitive#statusline()}
endif

" Disable visualbell
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif

""" Netrw config
" no error when showing NetrwSettings
let g:netrw_localrmdir = ''
let g:netrw_keepdir       = 0  " i.e. pwd = current browsing directory

""*****************************************************************************
""" 5 Plugins Settings
""*****************************************************************************

if has('win32') || has('win64')
    let g:os='win32'

    let g:rg_exe = g:homedirw32. '\bin\rg.exe'

    " gitgutter gvim
    let g:gitgutter_git_executable =  g:homedirw32. '\\workplace\\msys2\\usr\\bin\\git.exe'
    let g:gitgutter_enabled =0

    let g:fugitive_git_executable=g:gitgutter_git_executable
    " zoom plugin for gvim
    " https://www.vim.org/scripts/script.php?script_id=2321
    let s:save_cpo = &cpo
    set cpo&vim

    " keep default value
    let s:current_font = &guifont

    " gitgutter quickfixlist
    command! Gqf GitGutterQuickFix | copen


    "" Set shell to powershell
    command! -narg=0 PsDesktop :call s:PowerShell()

    function! s:PowerShell()
        " desktop
        set shell=powershell.exe
        set shellcmdflag=-Command
        set shellxquote="

    "" if &shell ==# 'powershell'
        set background=dark
        highlight Normal guibg=#012456
    "" endif

    endfunction
    " command
    command! -narg=0 ZoomIn    :call s:ZoomIn()
    command! -narg=0 ZoomOut   :call s:ZoomOut()
    command! -narg=0 ZoomReset :call s:ZoomReset()

    " map
    nmap + :ZoomIn<CR>
    nmap - :ZoomOut<CR>

    " guifont size + 1
    function! s:ZoomIn()
      let l:fsize = substitute(&guifont, '^.*:h\([^:]*\).*$', '\1', '')
      let l:fsize += 1
      let l:guifont = substitute(&guifont, ':h\([^:]*\)', ':h' . l:fsize, '')
      let &guifont = l:guifont
    endfunction

    " guifont size - 1
    function! s:ZoomOut()
      let l:fsize = substitute(&guifont, '^.*:h\([^:]*\).*$', '\1', '')
      let l:fsize -= 1
      let l:guifont = substitute(&guifont, ':h\([^:]*\)', ':h' . l:fsize, '')
      let &guifont = l:guifont
    endfunction

    " reset guifont size
    function! s:ZoomReset()
      let &guifont = s:current_font
    endfunction

    let &cpo = s:save_cpo
else

    let g:os="unix"
    let g:rg_exe = '~/bin/rg.exe'
    " block cursor normal mode
    set guicursor=n-v-c:block-Cursor  

    set termwintype=winpty
    set termwinsize=10*0
endif

" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" IndentLine
let g:indentLine_enabled = 1
let g:indentLine_concealcursor = 0
let g:indentLine_char = '┆'
let g:indentLine_faster = 1

"" w3m

let g:w3m_binary_path = g:homedirw32.'\\workplace\\msys2\\usr\\bin'
let g:w3m_start_page = 'https://tessarinseve.pythonanywhere.com/nws/index.html'

" GoTo code navigation.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
"" coc sets default home to ~/vimfiles
let g:coc_config_home = '~/.vim'
let g:coc_disable_startup_warning = 1

if has('win32') || has('win64')
    let g:coc_node_path = g:homedirw32 . 'workspace\toolbox\apps\node\node'
else
    let g:coc_node_path = g:homedirw32 . 'workspace/toolbox/apps/node/node'
endif

"" coc snippets
"" next placeholder
let g:coc_snippet_next = '<M-n>'

"" jump to previous placeholder
let g:coc_snippet_prev = '<M-b>'

imap <M-n> <Plug>(coc-snippets-expand-jump)

"" vim9grep 

"" let g:serpl_exe_path = "C:\\Users\\Seve\\workspace\\toolbox\\bin\\serpl.exe"


"" ----------------------- RG -----------------------

if executable(g:rg_exe)
    let &grepprg=g:rg_exe . ' -H --no-heading --vimgrep'
    set grepformat=%f:%l:%c:%m
    augroup rgquickfix
        autocmd!
        autocmd QuickFixCmdPost [^l]* cwindow
        autocmd QuickFixCmdPost l*    lwindow
    augroup END
endif
"" -------------------- vim notes config --------------------------------
let g:notes_directories = ['~/Documents/MyNotes',"~/.pubs/notes"]
let g:notes_tagsindex = 'tags'
" default misc/notes
let g:notes_indexfile = '~/Documents/MyNotes/index.pickle'
let g:notes_title_sync = 'rename_file'
let g:notes_suffix = '.txt'
let g:notes_unicode_enabled = 0
let g:notes_markdown_program = 'C:\Users\Seve\AppData\Local\Programs\WinDiction\pandoc.exe'

" integrate notes and pubs note and vim-highlighter
"
nnoremap <silent><leader>h1 <esc>:10 Hi+<CR>
xnoremap <silent><leader>h1 <esc>:10 Hi+x<CR>
nnoremap <silent><leader>h2 <esc>:14 Hi+<CR>
xnoremap <silent><leader>h2 <esc>:14 Hi+x<CR>
nnoremap <silent><leader>h <Cmd>Hi><CR>
augroup NoteGroup
    au!
    autocmd BufNewFile *.txt  :call CheckNoteFile()
    autocmd BufWrite *.txt :call SaveHL()
    " Something wrong in :Hi load 
    " autocmd BufReadPost *.txt call LoadHL()
    " not working when not a note dir
    autocmd BufWritePost,VimEnter join(g:notes_directories,'/*.txt')../*.txt 
                \   silent execute '!'.g:homedirw32.'\\bin\\uctags.exe -R -f notetags --options=C:\\Users\\Seve\\Documents\\MyNotes\\Notes.ctags' 
    autocmd VimEnter *.txt set tags=notetags

augroup END

function! CheckNoteFile()
   "custom syntax highlight
   highlight notesRealURL gui=underline
   let filename = expand("%:p:t")
   let basename = substitute(filename,".txt","","g")
   if (getline("1") !~ basename)
      exe "normal!ggO".basename
   endif
endfunction

function! SaveHL()
  silent exec "Hi save notes"
endfunction
function! LoadHL()
 silent exec "Hi load notes"
endfunction

" open cb_thunrdelink
nnoremap <silent> <M-0> <esc> :call Open_cb_thunderlink()<CR>
fun! Open_cb_thunderlink()
    let w = expand('<cWORD>')
    " check if a proper formatted cb_thunrdelink
    if w =~ '^\(cbthunderlink\|thunderlink\)://.\{-}[[:punct:]]$'
        echo 'Open '.w
        call setreg('*',w)
        execute dispatch#start_command(0,g:homedirw32.'\bin\open_cbthunderlink.exe',0,0)
    endif
endfun



set wildmenu
set wildmode=full
set wildoptions=pum,tagfile

"" wildignore config ignore files and dirs during fuzzy search
set wildignore+=tags,*.so,*.swp,*.log,*.db,*.exe 
set wildignore+=*.pickle,*.wav
set wildignore+=*.git*,*__pycache__*
set wildignore+=*node_modules*
 

"" session management
"nnoremap <leader>so :OpenSession<Space>
"nnoremap <leader>ss :SaveSession<Space>
"nnoremap <leader>sd :DeleteSession<CR>
"nnoremap <leader>sc :CloseSession<CR>

"let g:session_autosave = 'no'
"let g:session_autoload = 'no'



""" scratch.vim config
let g:scratch_horizontal =  0 "vertical split
let g:scratch_persistence_file = '~/.scratch_buffer'
let g:scratch_height = 40 "40 %

""" vim calendar integration with csvtodo
let g:calendar_monday = 1
let g:calendar_number_of_months = 2
let g:calendar_weeknm = 1 " WK01
let g:todofile = 'C:\\Users\\Seve\\Documents\\todo.csv'
function CheckCsvTodo(day,month,year,week,dir)
    let @" = ''
    if len(a:month) == 1
        let l:month_c = "0" . a:month
    else
        let l:month_c = a:month
    endif

    if len(a:day) == 1
        let l:day_c = "0" . a:day
    else
        let l:day_c = a:day
    endif
    let l:actionedaday = a:year . "-" . l:month_c. "-" . l:day_c
    let l:due = system("csvgrep.exe -c 8 -m " . l:actionedaday . " " . g:todofile)
    let l:tododuelist = split(l:due,'\r\n')
    if len(l:tododuelist) > 1
        let g:scratch_horizontal = 1
        call scratch#selection(1)
        let t_index = 0
        call append(0, l:actionedaday)
        for todoitem in l:tododuelist
            if t_index > 0
                let l:todo = split( todoitem ,",")
                let l:tododesc = "Project: " . l:todo[3] . "  " . l:todo[4] . "DUE TODAY"
                call append(t_index,tododesc)
            endif
            let t_index = t_index+1
        endfor
        let g:scratch_horizontal = 0
    else
        echom "Nothing to do:" . l:actionedaday
    endif

endfunction
"" let calendar_action = 'CheckCsvTodo'


""*****************************************************************************
""" 6 Commands
""*****************************************************************************
"" remove trailing whitespaces
command! FixWhitespace :%s/\s\+$//e
command! W write

""*****************************************************************************
""" 7 Functions
""*****************************************************************************
if !exists('*s:setupWrapping')
  function s:setupWrapping()
    set wrap
    set wm=2
    set textwidth=79
  endfunction
endif

""*****************************************************************************
""" 8 Autocmd Rules
""*****************************************************************************
augroup vimrc-sync-fromstart
  autocmd!
  autocmd BufEnter * :syntax sync maxlines=200
augroup END

""" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

""" txt
augroup vimrc-wrapping
  autocmd!
  autocmd BufRead,BufNewFile *.txt call s:setupWrapping()
augroup END

""" make/cmake
augroup vimrc-make-cmake
  autocmd!
  autocmd FileType make setlocal noexpandtab
  autocmd BufNewFile,BufRead CMakeLists.txt setlocal filetype=cmake
augroup END

""" taskjuggler
augroup filetypedetect
    au BufNewFile,BufRead *.tjp,*.tji setf tjp
augroup END

""" wiki
augroup wikift
    au BufRead,BufNewFile *.wiki set filetype=mediawiki
augroup END


""" vlang
augroup vlang
    au BufRead,BufNewFile *.v,*.vsh set filetype=vlang
augroup END

""" sapi voice files
augroup voice
    au BufRead,BufNewFile *.voice set filetype=voice
augroup END


"   https://vim.fandom.com/wiki/Autoselect_the_right_compiler_using_the_filetype

au BufRead *.py,*.v,*.vsh,*.vv try | execute "compiler ".&filetype | catch /./ | endtry
""*****************************************************************************
""" 9 Custom config
""*****************************************************************************

"" 9.1___________________________________Python________________________________
augroup vimrc-python
  autocmd!
  autocmd FileType python setlocal expandtab           " enter spaces when tab is pressed
              \ textwidth=79       " break lines when line length increases
              \ tabstop=4          " use 4 spaces to represent tab
              \ softtabstop=4
              \ shiftwidth=4        " number of spaces to use for auto indent
              \ autoindent          " copy indent from current line when starting a new line
              \ formatoptions+=croq
              \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with,match


augroup END

autocmd Filetype python nnoremap <buffer> <F6> :w<CR>: bel  terminal  python "%"<CR>

""" current buffer on python console
nnoremap <F7> :echo system('winpty python3 "' . expand('%') . '"')<cr>
nnoremap <silent><Leader>py :w <bar> :FloatermNew --height=0.4 --width=0.9 --wintype=float --name="pcr" --disposable --position=bottom --autoclose=0 py % <cr> 
nnoremap <silent><Leader>oc :Start C:\\Users\\Seve\\workplace\\octave\\octave-8.2.0-w64\\octave_cli.exe <cr> 
nnoremap <silent><Leader>sq :! C:\\Users\\Seve\\AppData\\Local\\Programs\\WinTermNote\\alacritty.exe --command C:\\Users\\Seve\\workplace\\msys2\\mingw64\\bin\\sqlite.exe <cr> 

""" floaterm abbreviations
cabbrev fn FloatermNew
nnoremap <silent><leader>th :floatermHide <cr>
nnoremap <silent><leader>tk :FloatermKill <cr>

""" windows msys2 terminals

"" if has('win32') || has('win64')

 let g:terminals_msys2_root = "C:\\Users\\Seve\\workplace\\msys2"
 let g:terminals_msys2_startingDirectory = expand("%:p:h")

"" endif


"" Syntax highlight
let python_highlight_all = 1


"" Installed Windows python version must be the same python linked when vim was
"" compiled.
"" Python3/Dyn option, see :version

"" !! vim90 needs python310.dll

if has('win32') || has('win64')
    set pythonthreedll=g:homedirw32.'\\appdata\\local\\programs\\python\\python311\\python311.dll'

else
    let s:uname = substitute(system('uname'), '\n', '', '')
    let user = substitute(system('whoami'), '\n', '', '')
    let s:wherepy3 = (system('where python'))
    if stridx(s:wherepy3, "AppData")>1
        set pythonthreedll=~/appdata/local/programs/python/python39/python39.dll
    endif
endif
"" https://github.com/yegappan/lsp/issues/228

if has('win32') || has('win64')
    echom "Starting LSPs"
    nnoremap gd :LspGotoDefinition<cr>
    nnoremap gr :LspPeekReference<cr>
    nnoremap K :LspHover<cr>
    highlight Pmenu guibg=darkblue   
    let lspOpts = #{
                \ autoHighlight: v:true,
                \ autoHighlightDiags: v:true,
                \ autoComplete: v:true,
                \ snippetSupport: v:true,
                \ ultisnipsSupport: v:false,
                \ noNewlineInCompletion: v:true,
                \}
    silent! autocmd User LspSetup call LspOptionsSet(lspOpts)

    "" Ruff Lsp Start/AddServer
    let lspServers = [#{
                \    name: 'pyright',
                \    filetype: ['python'],
                "\    path: 'pylsp.exe',
                "\    args: [''],
                "\    path: 'C:\\Users\Seve\\workspace\\toolbox\\bin\\ruff.exe',
                "\    args: ['server','--preview'],
                "\    initializationoptions: { "python.pythonPath" : "C:\\Users\\Seve\\AppData\\Local\\Programs\\Python\\Python311\\Lib\\site-packages"}, 
                "\    path: 'C:\\Users\\Seve\\workspace\\toolbox\\apps\\node\\node_modules\\.bin\\pyright-langserver',
                "\    args: ['--stdio'],
                \    path: 'jedi-language-server.exe',
                \    args: [''],
                \ },
                \#{
                \    name: 'py-ruff',
                \    filetype: ['python'],
                \    path: 'C:\\Users\Seve\\workspace\\toolbox\\bin\\ruff.exe',
                \    args: ['server'],
                \ },
                \#{
                \    name: 'v-analyzer',
                \    filetype: ['vlang'],
                \    path: 'C:\\Users\Seve\\workspace\\toolbox\\bin\\v-analyzer.exe',
                \    args: ['','--stdio'],
                \ },
                \#{
                \    name: 'pygls',
                \    filetype: ['mediawiki','markdown'],
                \    path: 'py',
                \    args: ['lsp.py'],
                \ },
                \#{
                \    name: 'taplo',
                \    filetype: ['toml'],
                \    path: 'C:\\Users\Seve\\workspace\\toolbox\\bin\\taplo.exe',
                \    args: ['lsp','stdio'],
                \ },
                \#{
                \    name: 'typos',
                \    filetype: ['txt'],
                \    path: 'C:\\Users\Seve\\workspace\\toolbox\\bin\\typos-lsp.exe',
                \    args: [''],
                \ },
                \#{
                \    name: 'deno',
                \    filetype: ['javascript','typescript'],
                \    path: 'C:\\Users\Seve\\bin\\deno.exe',
                \    initializationoptions: { "enable": v:true, "lint": v:true, "unstable": v:false },
                \    args: ['lsp'],
                \ },
                \#{
                \    name: 'markdown-oxide',
                \    filetype: ['notes'],
                \    path: 'C:\\Users\Seve\\workspace\\toolbox\\bin\\markdown-oxide.exe',
                \    args: [''],
                \ },
                \#{
                \    name: 'ast-grep lsp',
                \    filetype: [''],
                \    path: 'C:\\Users\\Seve\\workspace\\toolbox\\pyvenv\\astgrep\\Scripts\\ast-grep.exe',
                \    args: ['lsp'],
                \    initializationOptions: {},
                \ },
                \#{
                \    name: 'markdown lsp',
                \    filetype: [''],
                \    path: 'C:\\Users\Seve\\workspace\\toolbox\\bin\\marksman.exe',
                \    args: ['server'],
                \    initializationOptions: {},
                \ }]

    silent! autocmd User LspSetup call LspAddServer(lspServers)
endif


""*****************************************************************************
"" status line
""*****************************************************************************


let g:airline_theme = 'powerlineish'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline_skip_empty_sections = 1

" vim-airline
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_section_y='%{airline#util#wrap(airline#parts#ffenc(),0)}|%{ObsessionStatus("S","$")}|%{&shell}'
let g:airline_section_z='%L(%p) %v %{g:os}'
" "'%p%%%#__accent_bold#%{g:airline_symbols.linenr}%l%#__restore__#%#__accent_bold#/%L%{g:airline_symbols.maxlinenr}%#__restore__#%#__accent_bold#%{g:airline_symbols.colnr}%v%#__restore__#

if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          =  ""    "'▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = ""   " '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif 


""*****************************************************************************
""  GNU Diction
""*****************************************************************************



let g:windiction_dir='C:\Users\Seve\AppData\Local\Programs\WinDiction'
let g:windiction_lang='eu'

function! GNUDiction()

    let l:currbuff =expand("%:p")
    let g:gnudiction_disp=''
    let g:gnudiction_disp='!start cmd /k cd '.g:windiction_dir.' & diction_linter.exe '.g:windiction_lang.' " '.l:currbuff.' "'
    exec g:gnudiction_disp
    let &errorformat = '%f:%l:%c:%m'
    let l:dictiontmpfile=g:windiction_dir.'\\.error_list'
    sleep 2
    execute 'silent! caddfile ' . l:dictiontmpfile
    execute 'copen'

endfunction

"" *************************************************************************
"" Global variables configuration
"" *************************************************************************

let g:typst_pdf_viewer = 'SumatraPDF.exe'

"" taskfalcon binary
let g:vim_falcon_exe = "C:\\Users\\Seve\\bin\\falcon.exe"

"" # git.exe should be included in the same PATH
"" # mimic tig.exe installed via git for windows
let g:vimtig_tig_exe = "C:\\Users\\Seve\\bin\\tig.bat" 

""  fd executable for fdminifuzzy plugin
let g:fd_exe = "C:\\\\Users\\\\seve\\\\bin\\\\fd.exe"


"" **** JUPYTEXT OPTIONS ******
let g:jupytext_fmt = 'py'
