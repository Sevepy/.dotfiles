## start file for python Interactive: REPL+ Notebooks
import sys
import pathlib
import rlcompleter

userlibpath = pathlib.Path(pathlib.Path.home(),"PythonUserLib")
sys.path.append(str(userlibpath))
import nbimported

#sys.meta_path.append(nbimported.NotebookFinder())
