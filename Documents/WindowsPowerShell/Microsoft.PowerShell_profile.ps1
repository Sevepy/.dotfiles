# User's home dir
$homeDir = $env:USERPROFILE
# $Env:SHELL = "powershell"
$Env:GIT_EDITOR = "vim"
$Env:EDITOR = "vim"
$Env:PYTHONPATH = "C:\Users\Seve\AppData\Local\Programs\Python\Python311\Lib\site-packages"
$LocalAppData = $env:LocalAppData
# Import-Module PSReadLine
# see https://superuser.com/questions/1717635/powershell-history-isnt-persistent-anymore
# Set-PSReadLineOption -EditMode Emacs # Has a bug


# 0. Complition
. "$homeDir\workspace\toolbox\cli_completion\rg.ps1"

# 1. Alias
Set-Alias qrencode "C:\Users\Seve\workplace\msys2\mingw64\bin\qrencode.exe"
Set-Alias soffice "C:\Program Files\LibreOffice\program\soffice.exe"
Set-Alias wat2wasm "$homeDir\workspace\toolbox\bin\wabt-1.0.35\bin\wat2wasm.exe"
Set-Alias pintora "$homeDir\node_modules\.bin\pintora.ps1"
Set-Alias vim "$homeDir\Vim\vim90\vim.exe"
Set-Alias vimps Start-VimWithPowershell
Set-Alias litexl "$homeDir\workspace\toolbox\apps\lite-xl\lite-xl.exe"
Set-Alias libreoffice lo
Set-Alias svgbob "$LocalAppData\Programs\Python\Python311\Lib\site-packages\markdown_svgbob\bin\svgbob_0.5.5_x86_64-Windows.exe"
Set-Alias pdf "$homeDir\workplace\msys2\mingw64\bin\evince-previewer.exe"


# 2. Env
$env:NO_COLOR = "true"
$HistoryPath = "$homedir\Documents\WindowsPowerShell\PSReadLine\ConsoleHost_history.txt"
$env:PATHEXT += ";.py"
# python.exe/pip.exe dir:
$env:PATH += ";$homeDir\AppData\Roaming\Python\Python311\Scripts"
# required by pipx and zoxide
$env:PATH += ";$homeDir\.local\bin"
# git.exe dir
$env:PAGER =  "bat.exe" # "pager.exe"
$env:PATH += ";$homeDir\workplace\msys2\usr\bin"
$env:PATH += ";$homeDir\bin"
$env:PATH += ";$homeDir\workspace\toolbox\bin"
$env:PATH += ";$homeDir\workspace\toolbox\gitrepos\PowerShellUtils"
$env:PATH += ";$homeDir\workspace\toolbox\apps\node\"
$env:PATH += ";$homeDir\workspace\toolbox\apps\v\"
$env:PATH += ";$homeDir\AppData\Local\Programs\WinDiction"
$env:PATH += ";$homeDir\workspace\toolbox\apps\node\node_modules\.bin"
$env:VIRTUALENV_HOME = "$HOME\workspace\toolbox\pyvenv"

# 3. Functions
function Start-VimWithPowershell {
& $homeDir\Vim\vim90\vim.exe -c 'PsDesktop' $args
}

function Open-MRUFile {
    vim (listmru.exe | fzf.exe --preview  'bat --color=always {}')
}

# 3.1 Set Prompt+ python VENVs
function prompt {
    $venv = Get-Command -Name python | Select-Object -ExpandProperty Path
    if ($venv -match '.*\\envs\\([^\\]+)\\python.exe') {
        $envName = $matches[1]
        "($envName) PS $(Get-Location | Split-Path -Leaf)> "
    } else {
        "PS $(Get-Location | Split-Path -Leaf)> "
    }

     # Check if the subdirectory "toolbox" exists (no working)
    $toolboxExists = Test-Path -Path "toolbox" -PathType Container

    if ($toolboxExists) {
        $prompt += "[\033[96m(t)\033[0m] "  
    }

    $prompt += "PS $(Get-Location | Split-Path -Leaf)> "
    $prompt
}
###################################################################


function Invoke-Vim {
    param(
        [string]$Arguments,
        [string]$File,
        [string]$Tag,
        [string]$ErrorFile
    )

    $defaultArgs = "" #  "--cmd 'set shell=powershell'"
    $vimExePath = "$homeDir\Vim\vim90\vim.exe" # Replace with the actual path to vim.exe
    # $fullArgs = "$vimExePath $defaultArgs".Trim()
    # Write-Output $fullArgs

    if ($PSBoundParameters.ContainsKey('File') -and $File -eq '-') {
        # Read text from stdin
        $input | & $vimExePath $defaultArgs $Arguments
    }
    elseif ($PSBoundParameters.ContainsKey('Tag')) {
        # Edit file where tag is defined
        & $vimExePath $defaultArgs $Arguments -t $Tag
    }
    elseif ($PSBoundParameters.ContainsKey('ErrorFile')) {
        # Edit file with first error
        & $vimExePath $defaultArgs $Arguments -q $ErrorFile
    }
    else {
        # Edit specified file(s)
        & $vimExePath $defaultArgs $Arguments $File
    }
}

function lo() {
    param(
        [string]$file
        )

    # Get the current working folder
        $folder = Get-Location

    # Combine the folder and the file name into a full path
        $path = Join-Path $folder $file

    # Open the file with LibreOffice
        Start-Process -FilePath "C:\Program Files\LibreOffice\program\soffice.exe" -ArgumentList "--nologo", $path

}

Invoke-Expression (& { (zoxide init --cmd j powershell | Out-String) })


function Get-ProgID {
    param()
    $paths = @("REGISTRY::HKEY_CLASSES_ROOT\CLSID")
    if ($env:Processor_Architecture -eq "amd64") {
        $paths += "REGISTRY::HKEY_CLASSES_ROOT\Wow6432Node\CLSID"
    }
    Get-ChildItem $paths -include VersionIndependentPROGID -recurse |
        Select-Object @{
            Name='ProgID'
            Expression={$_.GetValue("")}
        }, @{
            Name='32Bit'
            Expression={
                if ($env:Processor_Architecture -eq "amd64") {
                    $_.PSPath.Contains("Wow6432Node")
                } else {
                    $true
                }
            }
        }
}
