
## Windows Dotfiles management without symlinks

This repository contains my personal dotfiles for Bash, PowerShell, Vim and other important tools.

More info can be found here:
https://tessarinseve.pythonanywhere.com/nws/2021-01-25.wiki.html


### Bash Shell Configuration
    
    .bashrc


### Vim Configuration

    .vimrc


