#!/usr/bin/env python

#MIT License

#Copyright (c) 2020 Seve Tessarin

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.


import os
import shutil
import sys
from pathlib import Path

if __name__ == '__main__':
    try:
        if sys.argv[1] == 'remove':
            remove_module = True
    except IndexError:
         remove_module = False

    ahk_files = ['keyboard_hotkeys.ahk']

    path_list =["AppData","Roaming","Microsoft","Windows","Start Menu","Programs","Startup"]

    win_start_folder = Path.home().joinpath("\\".join(path_list)) 
    ahk_hidden_folder = Path.home().joinpath(".AHKscripts")
    dest = Path(win_start_folder,"keyboard_hotkeys.ahk")
    src = Path(ahk_hidden_folder,"keyboard_hotkeys.ahk") 
    if remove_module:
        os.remove(dest)
        print("\n ahk configuration removed")
    else:
        print("\n ahk configuration installed")
        shutil.copy(src, dest)
