#!/usr/bin/env bash
shopt -s expand_aliases
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


git clone --bare https://gitlab.com/Sevepy/.dotfiles $HOME/.dotfiles

dotfiles checkout -f master
