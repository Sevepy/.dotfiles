#!/usr/bin/env bash
shopt -s expand_aliases
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


dotfiles ls-tree -r master --name-only  | xargs rm -f

## remove the side directory

rm -rf ~/.dotfiles

